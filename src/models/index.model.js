import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const IndexSchema = new Schema(
  {
    current: {
      type: Number,
      required: [true, 'currentEvent is required.'],
    },
  },
  {
    timestamps: true,
  }
);

const Index = mongoose.model('index', IndexSchema);

export default Index;
