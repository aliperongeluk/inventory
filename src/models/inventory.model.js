/**
 * @swagger
 * definitions:
 *  Inventory:
 *    type: object
 *    properties:
 *      product:
 *        type: string
 *      amount:
 *        type: integer
 */
