import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const EventSchema = new Schema(
  {
    type: {
      type: String,
      required: [true, 'type is required.'],
    },
    product: {
      type: Schema.Types.ObjectId,
      required: [true, 'product is required.'],
    },
    amount: {
      type: Number,
      required: [true, 'amount is required.'],
    },
  },
  {
    timestamps: true,
  }
);

const Event = mongoose.model('event', EventSchema);

export default Event;

/**
 * @swagger
 * definitions:
 *  EventPost:
 *    type: object
 *    properties:
 *      product:
 *        type: string
 *      amount:
 *        type: integer
 */

/**
 * @swagger
 * definitions:
 *  Event:
 *    type: object
 *    properties:
 *      _id:
 *        type: string
 *      type:
 *        type: string
 *      product:
 *        type: string
 *      amount:
 *        type: integer
 *      createdAt:
 *        type: string
 *        format: date-time
 *      updatedAt:
 *        type: string
 *        format: date-time
 *      __v:
 *        type: integer
 */
