import mongoose from 'mongoose';
const Schema = mongoose.Schema;

const SnapshotSchema = new Schema(
  {
    date: {
      type: Date,
      required: [true, 'date is required.'],
    },
    data: {
      type: Array,
      required: [true, 'data is required.'],
    },
  },
  {
    timestamps: true,
  }
);

const Snapshot = mongoose.model('snapshot', SnapshotSchema);

export default Snapshot;

/**
 * @swagger
 * definitions:
 *  Snapshot:
 *    type: object
 *    properties:
 *      data:
 *        type: array
 *        items:
 *          $ref: '#/definitions/Inventory'
 *      _id:
 *        type: string
 *      date:
 *        type: string
 *        format: date-time
 *      createdAt:
 *        type: string
 *        format: date-time
 *      updatedAt:
 *        type: string
 *        format: date-time
 *      __v:
 *        type: integer
 */
