import {
  NEW_PRODUCT,
  ADD_PRODUCT,
  REDUCE_PRODUCT,
} from '../environment/constants/eventTypes';
import Index from '../models/index.model';
import Snapshot from '../models/snapshot.model';
import Event from '../models/event.model';

export const buildInventoryFromEvents = (events, startingInventory = []) =>
  events.reduce((inventory, event) => {
    if (event.type === NEW_PRODUCT) {
      return newProduct(event, inventory);
    }

    const index = inventory.findIndex(
      element => element.product.toString() === event.product.toString()
    );

    if (event.type === ADD_PRODUCT) {
      return addProduct(event, inventory, index);
    }

    if (event.type === REDUCE_PRODUCT) {
      return reduceProduct(event, inventory, index);
    }
  }, startingInventory);

const newProduct = (event, inventory) => {
  inventory.push({ product: event.product, amount: event.amount });
  return inventory;
};

const addProduct = (event, inventory, index) => {
  inventory[index].amount += event.amount;
  return inventory;
};

const reduceProduct = (event, inventory, index) => {
  event.amount = -event.amount;
  return addProduct(event, inventory, index);
};

export const addOneToIndex = async () => {
  const index = await Index.findOne();
  index.current = index.current + 1;
  await index.save();
};

const getCurrentIndex = async () => {
  const index = await Index.findOne();
  return index.current;
};

export const timeForSnapshot = async () => {
  const index = await getCurrentIndex();
  return index >= 5;
};

export const createSnapshot = async () => {
  const events = await Event.find();
  const data = buildInventoryFromEvents(events);
  const date = events[events.length - 1].createdAt;
  const snapshot = new Snapshot({
    date: date,
    data: data,
  });
  await snapshot.save();

  // eslint-disable-next-line no-console
  console.log('Created snapshot...');

  await resetIndex();
};

const resetIndex = async () => {
  const index = await Index.findOne();
  index.current = 0;
  await index.save();
};
