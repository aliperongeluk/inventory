import swaggerJsDoc from 'swagger-jsdoc';

const options = {
  swaggerDefinition: {
    info: {
      title: 'Inventory Management',
      version: '1.0',
      description:
        'Swagger documentation for Inventory Management microservice (AliPerOngeluk).',
    },
  },

  apis: ['./src/routes/*.js', './src/models/*.js'],
};

const specs = swaggerJsDoc(options);

export default specs;
