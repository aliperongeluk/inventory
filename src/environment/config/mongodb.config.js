import dotenv from 'dotenv';
dotenv.config();

const {
  MONGO_USERNAME,
  MONGO_PASSWORD,
  MONGO_HOSTNAME,
  MONGO_PORT,
  MONGO_DB,
} = process.env;

export const dbUrl =
  'mongodb://' +
  MONGO_USERNAME +
  ':' +
  MONGO_PASSWORD +
  '@' +
  MONGO_HOSTNAME +
  ':' +
  MONGO_PORT +
  '/' +
  MONGO_DB +
  '?authSource=admin';
