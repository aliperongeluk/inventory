import EurekaConfig from 'eureka-js-client';
import ip from 'ip';

const ipAddr = process.env.IP_ADDRESS || ip.address();
const hostName = process.env.HOST_NAME || ip.address();
const port = process.env.PORT || 7001;

const eurekaClient = new EurekaConfig({
  // application instance information
  instance: {
    app: 'inventory-management',
    hostName: hostName,
    ipAddr: ipAddr,
    homePageUrl: `http://${ipAddr}:${port}`,
    port: {
      $: port,
      '@enabled': 'true',
    },
    vipAddress: 'inventory-management',
    dataCenterInfo: {
      '@class': 'com.netflix.appinfo.InstanceInfo$DefaultDataCenterInfo',
      name: 'MyOwn',
    },
    leaseInfo: {
      renewalIntervalInSecs: 10,
      durationInSecs: 30,
    },
  },
  eureka: {
    preferIpAddress: true,
    maxRetries: 30,
    registerWithEureka: true,
    fetchRegistry: true,
    serviceUrls: {
      default: [
        'http://eurekaserver-1:8761/eureka/apps/',
        'http://eurekaserver-2:8762/eureka/apps/',
      ],
    },
  },
});

export default eurekaClient;
