const constants = {
  EXCHANGE_INVENTORY: 'inventory',
  EXCHANGE_PRODUCTS: 'product-catalog',
  EXCHANGE_ORDER: 'order',
  SERVICE_NAME: 'inventory-management',
};

module.exports = constants;
