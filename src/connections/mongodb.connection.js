import mongoose from 'mongoose';
import { dbUrl } from '../environment/config/mongodb.config';
import Index from '../models/index.model';

mongoose.Promise = global.Promise;

mongoose.connect(dbUrl, { useNewUrlParser: true });
const connection = mongoose.connection
  .once('open', () => {
    console.log('Connected to Mongo on ' + dbUrl);
    initIndexCollection();
  })
  .on('error', error => {
    console.warn('Warning', error.toString());
  });

const initIndexCollection = () => {
  Index.findOne().then(index => {
    if (index === null) {
      const index = new Index({
        current: 0,
      });
      index.save().then();
    }
  });
};

export default connection;
