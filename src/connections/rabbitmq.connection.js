// LOCAL: amqp://localhost
// SERVER: amqp://AliB:perongelukexpres@rabbitmq

import amqp from 'amqplib';
let counter = 0;

const constants = require('../environment/constants/constants');

const connect = () => {
  return new Promise((resolve, reject) => {
    amqp
      .connect('amqp://AliB:perongelukexpres@rabbitmq')
      .then(connection => {
        counter = 0;

        const createReceiveChannel = new Promise((resolve, reject) => {
          connection
            .createChannel()
            .then(receiveChannel => {
              resolve(receiveChannel);
            })
            .catch(error => {
              reject(error);
            });
        });

        Promise.all([createReceiveChannel]).then(channels => {
          resolve({
            receiveChannel: channels[0],
          });
        });
      })
      .catch(error => {
        if (counter == 10) {
          reject(error);
        }

        setTimeout(() => {
          counter++;
          connect();
        }, 3000);
      });
  });
};

module.exports = { connect };
