const constants = require('../../environment/constants/constants');
const events = require('../../environment/constants/events');

import { NEW_PRODUCT } from '../../environment/constants/eventTypes';
import {
  createSnapshot,
  timeForSnapshot,
  addOneToIndex,
} from '../../utils/events';

import Event from '../../models/event.model';

const consumeProductCreated = messageChannel => {
  messageChannel
    .assertQueue(`${constants.SERVICE_NAME}_${events.PRODUCT_CREATED}`, {
      exclusive: false,
    })
    .then(queue => {
      messageChannel.bindQueue(
        queue.queue,
        constants.EXCHANGE_PRODUCTS,
        events.PRODUCT_CREATED
      );

      messageChannel.consume(
        queue.queue,
        async message => {
          const messageObject = JSON.parse(message.content.toString());
          const { _id } = messageObject;
          const event = new Event({
            type: NEW_PRODUCT,
            product: _id,
            amount: 0,
          });

          await event.save();
          await addOneToIndex();

          if (await timeForSnapshot()) createSnapshot().then();
        },
        {
          noAck: true,
        }
      );
    })
    .catch(error => {
      // eslint-disable-next-line no-console
      console.log(error);
    });
};

const consume = messageChannel => {
  consumeProductCreated(messageChannel);
};

module.exports = { consume };
