const constants = require('../../environment/constants/constants');
const events = require('../../environment/constants/events');

import {
  ADD_PRODUCT,
  REDUCE_PRODUCT,
} from '../../environment/constants/eventTypes';
import {
  createSnapshot,
  timeForSnapshot,
  addOneToIndex,
} from '../../utils/events';

import Event from '../../models/event.model';

const consumeOrderCreated = messageChannel => {
  messageChannel
    .assertQueue(`${constants.SERVICE_NAME}_${events.ORDER_CREATED}`, {
      exclusive: false,
    })
    .then(queue => {
      messageChannel.bindQueue(
        queue.queue,
        constants.EXCHANGE_ORDER,
        events.ORDER_CREATED
      );

      messageChannel.consume(
        queue.queue,
        async message => {
          const messageObject = JSON.parse(message.content.toString());
          messageObject.products.forEach(async product => {
            const { productId, amount } = product;
            const event = new Event({
              type: REDUCE_PRODUCT,
              product: productId,
              amount: amount,
            });

            await event.save();
            await addOneToIndex();

            if (await timeForSnapshot()) createSnapshot().then();
          });
        },
        {
          noAck: true,
        }
      );
    })
    .catch(error => {
      // eslint-disable-next-line no-console
      console.log(error);
    });
};

const consumeOrderCancelled = messageChannel => {
  messageChannel
    .assertQueue(`${constants.SERVICE_NAME}_${events.ORDER_CANCELLED}`, {
      exclusive: false,
    })
    .then(queue => {
      messageChannel.bindQueue(
        queue.queue,
        constants.EXCHANGE_ORDER,
        events.ORDER_CANCELLED
      );

      messageChannel.consume(
        queue.queue,
        async message => {
          const messageObject = JSON.parse(message.content.toString());
          messageObject.products.forEach(async product => {
            const { productId, amount } = product;
            const event = new Event({
              type: ADD_PRODUCT,
              product: productId,
              amount: amount,
            });

            await event.save();
            await addOneToIndex();

            if (await timeForSnapshot()) createSnapshot().then();
          });
        },
        {
          noAck: true,
        }
      );
    })
    .catch(error => {
      // eslint-disable-next-line no-console
      console.log(error);
    });
};

const consume = messageChannel => {
  consumeOrderCreated(messageChannel);
  consumeOrderCancelled(messageChannel);
};

module.exports = { consume };
