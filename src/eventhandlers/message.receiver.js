const constants = require('../environment/constants/constants');
const productConsumers = require('./consumers/product.consumers');
const orderConsumers = require('./consumers/order.consumers');
let messageChannel;

const startConsuming = () => {
  createExchanges();
  productConsumers.consume(messageChannel);
  orderConsumers.consume(messageChannel);
};

const createExchanges = () => {
  messageChannel.assertExchange(constants.EXCHANGE_PRODUCTS, 'topic', {
    durable: true,
  });
  messageChannel.assertExchange(constants.EXCHANGE_ORDER, 'topic', {
    durable: true,
  });
};

const setMessageChannel = channel => {
  messageChannel = channel;
  startConsuming();
};

module.exports = { setMessageChannel };
