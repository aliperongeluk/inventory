import express from 'express';
import Snapshot from '../models/snapshot.model';
import { sendSomethingWentWrongResponse } from '../utils/response';
const routes = express.Router();

/**
 * @swagger
 * /api/inventory-management/snapshot:
 *    get:
 *     tags:
 *       - Snapshot
 *     description: Retrieve all snapshots.
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *          description: Snapshots are returned.
 *          schema:
 *            type: array
 *            items:
 *              $ref: '#/definitions/Snapshot'
 *       400:
 *          description: Something went wrong.
 */
routes.get('/', async (req, res) => {
  try {
    const snapshots = await Snapshot.find();
    res.send(snapshots);
  } catch (e) {
    sendSomethingWentWrongResponse(res);
  }
});

export default routes;
