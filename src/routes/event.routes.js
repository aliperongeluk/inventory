import express from 'express';
const routes = express.Router();

import { sendSomethingWentWrongResponse } from '../utils/response';
import {
  NEW_PRODUCT,
  ADD_PRODUCT,
  REDUCE_PRODUCT,
} from '../environment/constants/eventTypes';

import {
  createSnapshot,
  timeForSnapshot,
  addOneToIndex,
} from '../utils/events';

import Event from '../models/event.model';
import Index from '../models/index.model';

/**
 * @swagger
 * /api/inventory-management/event:
 *    get:
 *     tags:
 *       - Events
 *     description: All events.
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *          description: All events are returned.
 *          schema:
 *            type: array
 *            items:
 *              $ref: '#/definitions/Event'
 *       400:
 *          description: Something went wrong.
 */
routes.get('/', async (req, res) => {
  try {
    const { date } = req.query;
    const query = date ? { createdAt: { $lte: date } } : {};
    const event = await Event.find(query).sort({ createdAt: -1 });

    res.send(event);
  } catch (e) {
    sendSomethingWentWrongResponse(res);
  }
});

/**
 * @swagger
 * /api/inventory-management/event/index:
 *    get:
 *     tags:
 *       - Events
 *     description: Show current index of events.
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *          schema:
 *            type: integer
 *       400:
 *          description: Something went wrong.
 */
routes.get('/index', async (req, res) => {
  try {
    const index = await Index.findOne();
    res.send(index.current.toString());
  } catch (e) {
    sendSomethingWentWrongResponse(res);
  }
});

/**
 * @swagger
 * /api/inventory-management/event/new:
 *    post:
 *     tags:
 *       - Events
 *     parameters:
 *      - name: event
 *        description: Event object
 *        in:  body
 *        required: true
 *        type: string
 *        schema:
 *          $ref: '#/definitions/EventPost'
 *     description: Add a new product.
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *          description: Success message.
 *       400:
 *          description: Something went wrong.
 */
routes.post('/new', async (req, res) => {
  try {
    const { product, amount } = req.body;
    const event = new Event({
      type: NEW_PRODUCT,
      product: product,
      amount: amount,
    });

    await event.save();
    await addOneToIndex();

    if (await timeForSnapshot()) createSnapshot().then();

    res.send(`Created product ${product} with starting amount ${amount}`);
  } catch (e) {
    sendSomethingWentWrongResponse(res);
  }
});

/**
 * @swagger
 * /api/inventory-management/event/add:
 *    post:
 *     tags:
 *       - Events
 *     parameters:
 *      - name: event
 *        description: Event object
 *        in:  body
 *        required: true
 *        type: string
 *        schema:
 *          $ref: '#/definitions/EventPost'
 *     description: Add stock for a product.
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *          description: Success message.
 *       400:
 *          description: Something went wrong.
 */
routes.post('/add', async (req, res) => {
  try {
    const { product, amount } = req.body;
    const event = new Event({
      type: ADD_PRODUCT,
      product: product,
      amount: amount,
    });

    await event.save();
    await addOneToIndex();

    if (await timeForSnapshot()) createSnapshot().then();

    res.send(`Added ${amount} item(s) for product ${product}`);
  } catch (e) {
    sendSomethingWentWrongResponse(res);
  }
});

/**
 * @swagger
 * /api/inventory-management/event/reduce:
 *    post:
 *     tags:
 *       - Events
 *     parameters:
 *      - name: event
 *        description: Event object
 *        in:  body
 *        required: true
 *        type: string
 *        schema:
 *          $ref: '#/definitions/EventPost'
 *     description: Remove stock for a product.
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *          description: Success message.
 *       400:
 *          description: Something went wrong.
 */
routes.post('/reduce', async (req, res) => {
  try {
    const { product, amount } = req.body;
    const event = new Event({
      type: REDUCE_PRODUCT,
      product: product,
      amount: amount,
    });

    await event.save();
    await addOneToIndex();

    if (await timeForSnapshot()) createSnapshot().then();

    res.send(`Removed ${amount} item(s) for product ${product}`);
  } catch (e) {
    sendSomethingWentWrongResponse(res);
  }
});

export default routes;
