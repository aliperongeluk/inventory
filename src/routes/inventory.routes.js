import express from 'express';
const routes = express.Router();

import { buildInventoryFromEvents } from '../utils/events';
import { sendSomethingWentWrongResponse } from '../utils/response';

import Event from '../models/event.model';
import Snapshot from '../models/snapshot.model';

/**
 * @swagger
 * /api/inventory-management/inventory:
 *    get:
 *     tags:
 *       - Inventory
 *     description: Retrieve the full inventory.
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *          description: Products are returned with amount of stock.
 *          schema:
 *            type: array
 *            items:
 *              $ref: '#/definitions/Inventory'
 *       400:
 *          description: Something went wrong.
 */
routes.get('/', async (req, res) => {
  try {
    const { date: dateQuery } = req.query;

    const snapshot = (await getSnapshot(dateQuery)) || {};
    const { data: snapshotData = [], date: snapshotDate } = snapshot;

    const inventory = buildInventoryFromEvents(
      await getEvents(createQuery(snapshotDate, dateQuery)),
      snapshotData
    );
    res.send(inventory);
  } catch (e) {
    sendSomethingWentWrongResponse(res);
  }
});

/**
 * @swagger
 * /api/inventory-management/inventory/{id}:
 *    get:
 *     tags:
 *       - Inventory
 *     description: Retrieve the full inventory.
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *          type: string
 *         required: true
 *         description: String ID of the product.
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *          description: Products are returned with amount of stock.
 *          schema:
 *            $ref: '#/definitions/Inventory'
 *       400:
 *          description: Something went wrong.
 */
routes.get('/:id', async (req, res) => {
  try {
    const { date: dateQuery } = req.query;
    const id = req.params.id;

    const snapshot = (await getSnapshot(dateQuery)) || {};
    const { data: snapshotData = [], date: snapshotDate } = snapshot;

    const product =
      snapshotData[snapshotData.findIndex(snap => snap.product === id)];

    const inventory = buildInventoryFromEvents(
      await getEvents(createQuery(snapshotDate, dateQuery, id)),
      product ? [product] : []
    );
    res.send(inventory[0]);
  } catch (e) {
    sendSomethingWentWrongResponse(res);
  }
});

const getSnapshot = async (date = null) =>
  await Snapshot.findOne(date ? { date: { $lte: date } } : {}).sort({
    created_at: -1,
  });

const createQuery = (snapshotDate, queryDate, productId = null) => {
  const query = {};
  if (snapshotDate || queryDate) {
    query.createdAt = {};
    if (snapshotDate) {
      query.createdAt.$gt = snapshotDate;
    }
    if (queryDate) {
      query.createdAt.$lte = queryDate;
    }
  }

  if (productId) {
    query.product = productId;
  }
  return query;
};

const getEvents = async (query = {}) => await Event.find(query);

export default routes;
