FROM node:10.13.0

WORKDIR /home/docktor/compositions/inventory-management

COPY package*.json ./

RUN npm install
RUN npm install pm2 -g

COPY . .

RUN npm run build

EXPOSE 7001

CMD ["pm2-runtime", "dist/server.js"]
