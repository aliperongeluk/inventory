// eslint-disable-next-line no-unused-vars
import mongo from './src/connections/mongodb.connection';
import rabbitmq from './src/connections/rabbitmq.connection';
import swaggerUi from 'swagger-ui-express';
import swaggerEnv from './src/environment/config/swagger';
import eurekaClient from './src/environment/config/eureka.config';

import messageReceiver from './src/eventhandlers/message.receiver';

import express from 'express';
const app = express();

const port = process.env.PORT || 7001;

import inventoryRoute from './src/routes/inventory.routes';
import eventRoute from './src/routes/event.routes';
import snapshotRoute from './src/routes/snapshot.routes';

import bodyParser from 'body-parser';

// body-parser:
app.use(bodyParser.json());

// routes:
app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerEnv));
app.use('/inventory', inventoryRoute);
app.use('/event', eventRoute);
app.use('/snapshot', snapshotRoute);

// default:
app.use('*', (req, res) => {
  res.status(404).send({ error: 'Not available' });
});

rabbitmq
  .connect()
  .then(channels => {
    messageReceiver.setMessageChannel(channels.receiveChannel);
  })
  .catch(err => {
    // eslint-disable-next-line no-console
    console.error(err);
  });

if (process.env.NODE_ENV === 'production') {
  // eslint-disable-next-line no-console
  console.log('Starting eureka connection...');
  eurekaClient.start();
}

app.listen(port, () => {
  // eslint-disable-next-line no-console
  console.log(`Running Inventory Management microservice on port [${port}]...`);
});
